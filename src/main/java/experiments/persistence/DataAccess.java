package experiments.persistence;

//DAO - data access object
public interface DataAccess
{
    public GroupPersistence AddGroup(GroupPersistence data);

    public GroupPersistence[] GetGroups();

    public GroupPersistence GetGroup(long id);

    public UserPersistence GetUser(String login);

    public MembershipPersistence[] GetUserMemberships(long userId);

    public MembershipPersistence[] GetGroupMemberships(long groupId);

    public MessagePersistence[] GetMessages(long groupId);

    public UserPersistence AddUser(UserPersistence data);

    public UserPersistence UpdateUser(UserPersistence data);

    public MessagePersistence AddMessage(MessagePersistence data);

    public MembershipPersistence AddMembership(MembershipPersistence data);
}
