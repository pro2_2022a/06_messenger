package experiments.persistence;

import java.time.Instant;

public class MessagePersistence
{
    private String text;
    private Instant time;
    private long userId;
    private long groupId;
    private long id;

    public void setId(long id)
    {
        this.id = id;
    }

    public long getId()
    {
        return id;
    }

    public MessagePersistence(String text, Instant time, long userId, long groupId, long id)
    {
        this.text = text;
        this.time = time;
        this.userId = userId;
        this.groupId = groupId;
        this.id = id;
    }

    public String getText()
    {
        return text;
    }

    public Instant getTime()
    {
        return time;
    }

    public long getUserId()
    {
        return userId;
    }

    public long getGroupId()
    {
        return groupId;
    }
}
