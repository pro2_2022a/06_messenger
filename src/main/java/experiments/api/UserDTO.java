package experiments.api;

public class UserDTO {
    private final String login;
    private String name;
    private long id;

    public UserDTO(String login, String name, long id)
    {
        this.login = login;
        this.name = name;
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public String getName()
    {
        return name;
    }

    public long getId()
    {
        return id;
    }
}
