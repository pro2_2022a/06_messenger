package experiments;

import experiments.api.ApiClient;
import swingClient.ClientMainFrame;

import javax.swing.*;
import java.io.*;
import java.util.Properties;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Properties config = new Properties();
        config.load(new FileInputStream("app.config"));
        String serverUrl = config.getProperty("serverUrl");
        Thread thread = new Thread(()->{
            try {
                Server.Listen();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        thread.start();

        ApiClient apiClient = new ApiClient(serverUrl);

        SwingUtilities.invokeLater(() -> {
            try {
                new ClientMainFrame(apiClient);
            } catch (Exception e) {
                System.out.println("GUI ERROR: "+e);
            }
        });
    }
}
