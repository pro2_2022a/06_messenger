package swingClient;

import experiments.api.ApiClient;
import experiments.api.UserDTO;

import javax.swing.table.AbstractTableModel;
import java.util.HashMap;

public class FriendsTableModel extends AbstractTableModel
{
    private final ApiClient apiClient;
    private Row[] rows = {
            new Row(0,0,0), // TODO: Smazat
            new Row(0,0,0), // TODO: Smazat
            new Row(0,0,0) // TODO: Smazat
    };

    public FriendsTableModel(ApiClient apiClient)
    {
        this.apiClient = apiClient;
    }

    public void reset(UserDTO user)
    {
        // TODO: stáhnout data pomocí apiClient a naplnit pole rows
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return rows.length;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return rows[rowIndex].id;
            case 1: return rows[rowIndex].commonGroupsCount;
            case 2: return rows[rowIndex].messagesCount;
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column)
    {
        switch (column)
        {
            case 0: return "Id";
            case 1: return "Shared groups";
            case 2: return "Received messages";
            default: return "";
        }
    }

    class Row
    {
        public Row(long id, int commonGroupsCount, int messagesCount)
        {
            this.id = id;
            this.commonGroupsCount = commonGroupsCount;
            this.messagesCount = messagesCount;
        }

        public long id;
        public int commonGroupsCount;
        public int messagesCount;
    }
}
