package swingClient;

import experiments.api.ApiClient;
import experiments.api.GroupDTO;

import javax.swing.table.AbstractTableModel;

public class ActiveGroupsTableModel extends AbstractTableModel
{
    ApiClient apiClient;

    public ActiveGroupsTableModel(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        String newName = aValue.toString();

        try
        {
            apiClient.PutGroup(new GroupDTO(newName,groups[rowIndex].getId()));
        }
        catch (Exception e)
        {
            return;
        }

        groups[rowIndex] = new GroupDTO(
                newName,
                groups[rowIndex].getId()
        );
    }

    private GroupDTO[] groups = new GroupDTO[0];

    public void reset(GroupDTO[] groups)
    {

        this.groups = groups;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return groups.length;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        switch (columnIndex)
        {
            case 0: return groups[rowIndex].getName();
            default: return "";
        }
    }
}
